#ifndef BUG_H
#define BUG_H

#include "Combat.h"
class Bug : public Combat {
private:
	const float speed = 50.0f;
public:
	Bug();
	Bug(int vida, float tiempo);
	~Bug();
	void Attack(sf::Time tempo, Character* spaider);
};

#endif