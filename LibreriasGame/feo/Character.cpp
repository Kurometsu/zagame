#include"Character.h"

Character::Character()
:
health(startingHealth),
score(0),
direction(0),
usingTexture(),
seinSprite(){
	
	usingTexture.loadFromFile("Assets/Spiders.png");
	rectSourceSprite.contains(32, 32);
	seinSprite.setTexture(usingTexture);
	seinSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
}
Character::Character(int life, int points, int direccion, sf::RectangleShape rectangular)
	:
	health(life),
	score(points),
	direction(direccion),
	usingTexture(){
	usingTexture.loadFromFile("Assets/Spider.png");
	rectSourceSprite.contains(32, 32);
	seinSprite.setTexture(usingTexture);
	seinSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
}
void Character::SetHealth(int life) {
	health = life;
}
void Character::SetScore(int points) {
	score = points;
}
void Character::Draw(sf::RenderWindow &ventana) {
	ventana.draw(seinSprite);
}
void Character::Move(sf::Time cuento) {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		direction = 0;
		seinSprite.move(0, -(cuento.asSeconds() * velocity));
		seinSprite.setTextureRect(sf::IntRect(0, animFrom2, 32, 32));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		direction = 2;
		seinSprite.move(0, cuento.asSeconds() * velocity);
		seinSprite.setTextureRect(sf::IntRect(0, animFrom2, 32, 32));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		direction = 3;
		seinSprite.move(-(cuento.asSeconds() * velocity), 0);
		seinSprite.setTextureRect(sf::IntRect(animFrom1, 0, 32, 32));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		direction = 1;
		seinSprite.move(cuento.asSeconds() * velocity, 0);
		seinSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	}
	}
void Character::Shoot(Web* tela) {
	if (tela != NULL) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
			if (tela->GetThrown() == false) {
				tela->GetShape().setPosition(seinSprite.getPosition());
				tela->SetLook(direction);
				tela->SetThrown(true);
			}
		}
	}
}
const bool Character::Death() {
	if (health <= 0) {
		return true;
	}
	return false;
}


void Character::SetDirection(int direccion){
	direction = direccion;
}

const int Character::GetHealth(){
	return health;
}
const int Character::GetScore(){
	return score;
}
const int Character::GetDirection(){
	return direction;
}
const sf::Sprite Character::GetShape(){
	return seinSprite;
}
const sf::IntRect Character::GetRect(){
	return rectSourceSprite;
}