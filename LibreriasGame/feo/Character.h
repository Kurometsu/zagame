#ifndef CHARACTER_H
#define CHARACTER_H

#include<iostream>
#include"SFML/Window.hpp"
#include"SFML/Graphics.hpp"
#include"Web.h"
using namespace std;
class Character {
private:
	sf::Texture usingTexture;
	sf::IntRect rectSourceSprite;
	sf::Sprite seinSprite;
	const int startingPosition = 100;
	const int velocity = 750;
	const int startingHealth = 110;
	const int spriteX = 46;
	const int spriteY = 64;
	const int animFrom1 = 33;
	const int animFrom2 = 66;
	const int animFrom4 = 135;
	int health;
	int score;
	int direction;
public:
	Character();
	Character(int life, int points, int direccion, sf::RectangleShape rectangular);
	void SetHealth(int life);
	void SetScore(int points);
	void SetDirection(int direccion);
	void Draw(sf::RenderWindow &ventana);
	void Move(sf::Time cuento);
	void Shoot(Web* piedra);
	const bool Death();
	const int GetHealth();
	const int GetScore();
	const int GetDirection();
	const sf::Sprite GetShape();
	const sf::IntRect GetRect();
};

#endif