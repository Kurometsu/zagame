#include "Combat.h"

Combat::Combat()
:
health(basicHealth),
zaTexture(),
bascSprite(),
enemyTime(0){
	zaTexture.loadFromFile("Assets/web.png");
	rectSourceSprite.contains(32, 32);
	bascSprite.setTexture(zaTexture);
	bascSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	sound.loadFromFile("Assets/Squish.wav");
	sonidito.setBuffer(sound);
}
Combat::Combat(int vida, float tiempo)
:
health(vida),
zaTexture(),
enemyTime(tiempo){
	zaTexture.loadFromFile("Assets/web.png");
	rectSourceSprite.contains(32, 32);
	bascSprite.setTexture(zaTexture);
	bascSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	sound.loadFromFile("Assets/Squish.wav");
	sonidito.setBuffer(sound);
}
Combat::~Combat(){
}
void Combat::Attack(sf::Time tempo, Character* spaider){
}
void Combat::SetHealth(int vida) {
	health = vida;
}
int Combat::GetHealth() {
	return health;
}
void Combat::Draw(sf::RenderWindow &ventana) {
	ventana.draw(bascSprite);
}
const bool Combat::Death(){
	if (health <= 0) {
		return true;
	}
	return false;
}
void Combat::posCombat(Character* asesino){
	if (Death()) {
		health = basicHealth;
		this->PositionNow(asesino);
	}
}
void Combat::SetEnemyTime(float timing) {
	enemyTime = timing;
}
const float Combat::GetEnemyTime() {
	return enemyTime;
}
void Combat::CollideShoot(Web* tela) {
	if (!Death()) {
		if (tela != NULL) {
			if (tela->GetShape().getGlobalBounds().intersects(bascSprite.getGlobalBounds())) {
				SetHealth(GetHealth() - GetHealth());
				tela->GetShape().setPosition(ditchedOutPosition, ditchedOutPosition);
				tela->SetThrown(false);
				sonidito.play();
			}
		}
	}
}
void Combat::PositionNow(Character* amenaza){
	positionOnMap = rand() % positionTry;
	if (positionOnMap != amenaza->GetShape().getPosition().x && positionOnMap != amenaza->GetShape().getPosition().y){
		if (amenaza->GetShape().getPosition().x > positionOnMap) {
			bascSprite.setPosition(positionOnMap + playerAway, positionOnMap);
		}
		else if (amenaza->GetShape().getPosition().x < positionOnMap) {
			bascSprite.setPosition(positionOnMap - playerAway, positionOnMap);
		}
		if (amenaza->GetShape().getPosition().y > positionOnMap) {
			bascSprite.setPosition(positionOnMap, positionOnMap + playerAway);
		}
		else if (amenaza->GetShape().getPosition().y < positionOnMap) {
			bascSprite.setPosition(positionOnMap, positionOnMap - playerAway);
		}
	}
}
const sf::Sprite& Combat::GetShape() {
	return bascSprite;
}
const sf::IntRect Combat::GetRect() {
	return rectSourceSprite;
}