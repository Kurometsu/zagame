#ifndef COMBAT_H
#define COMBAT_H

#include<iostream>
#include"SFML/Window.hpp"
#include"SFML/Graphics.hpp"
#include"SFML/Audio.hpp"
#include"Character.h"
#include"Web.h"
class Combat{
protected:
	sf::Texture zaTexture;
	sf::IntRect rectSourceSprite;
	sf::Sprite bascSprite;
	sf::SoundBuffer sound;
	sf::Sound sonidito;
	const int ditchedOutPosition = 1000;
	const int basicHealth = 20;
	const int playerAway = 30;
	const int positionTry = 800;
	int health;
	int positionOnMap;
	float enemyTime;
public:
	Combat();
	Combat(int vida, float tiempo);
	~Combat();
	virtual void Attack(sf::Time tempo, Character* spaider) = 0;
	void SetHealth(int vida);
	int GetHealth();
	void Draw(sf::RenderWindow &ventana);
	const bool Death();
	void posCombat(Character* winner);
	void SetEnemyTime(float timing);
	const float GetEnemyTime();
	void CollideShoot(Web* Tela);
	void PositionNow(Character* amenaza);
	const sf::Sprite& GetShape();
	const sf::IntRect GetRect();
};

#endif