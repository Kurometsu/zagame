#include "Game.h"

Game::Game()
:
score(0),
highScore(0){
	srand(time(0));
	spider = new Character();
	proyectile = new Web();
	for (int i = 0; i < TOPE; i++) 
	{
		if (i < TOPE / 2) {
			enemies[i] = new Wasp();
		}
		else if (i >= TOPE / 2) {
			enemies[i] = new Bug();
		}
	}
}
Game::~Game(){
	delete spider;
	delete proyectile;
	for (int i = 0; i < TOPE; i++)
	{
		if (enemies[i] != NULL)
		{
			delete enemies[i];
			enemies[i] = NULL;
		}
	}
	spider = NULL;
	proyectile = NULL;
}
void Game::Menu(){
	try{
		HostWeb.setHost("http://query.yahooapis.com");
		hostRequest.setUri("v1/public/yql?q=select%20item.condition.code%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22dallas%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
		seinResponse = HostWeb.sendRequest(hostRequest);
		datus = json::parse(seinResponse.getBody().c_str());
		string reportero = datus["query"]["results"]["channel"]["item"]["condition"]["code"];
		clima = stoi(reportero);
	}
	catch (exception error1){
		clima = CLIMA;
	}
	sf::RenderWindow window(sf::VideoMode(ventanaX, ventanaY), "Spider in her Web");
	points.open("puntos.txt");
	if (!points){
		ofstream creador;
		creador.open("puntos.txt");
		creador.close();
		points.open("puntos.txt");
	}
	if (points.is_open()){
		points >> pointsFind;
		highScore = atoi(pointsFind.c_str());
	}
	points.close();
	isPlaying = false;
	sf::Font normalFont;
	sf::Font fancyFont;
	normalFont.loadFromFile("Assets/Crimson-Roman.ttf");
	fancyFont.loadFromFile("Assets/Spooky-Spiders.ttf");
	sf::Text namae("Spider Web", fancyFont, 50);
	sf::Text scores("High score: " + pointsFind, normalFont, 30);
	sf::Text start("Press space to start", normalFont, 35);
	sf::Text instrucciones("Arrows to move ; W to fire", normalFont, 20);
	sf::Text extra("Press C for credits , V to close them",normalFont,20);
	namae.setFillColor(sf::Color::Black);
	start.setFillColor(sf::Color::Black);
	scores.setFillColor(sf::Color::Black);
	instrucciones.setFillColor(sf::Color::Black);
	extra.setFillColor(sf::Color::Black);

	namae.move(250.0f, 50.0f);
	scores.move(500.0f, 200.0f);
	start.move(180.0f, 320.0f);
	instrucciones.move(150.0f, 400.f);
	extra.move(100.0f, 490.f);
	while (window.isOpen()) 
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) 
			{
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				isPlaying = true;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
			{
				Credits(window);
			}
		}
		if (isPlaying == true) {
			Play(window);
			scores.setString("Highest score: " + to_string(highScore));
		}
		switch (clima) 
		{
			case sunny:
			window.clear(sf::Color::Yellow);
			break;
			case windy:
			window.clear(sf::Color::Blue);
			break;
			case cloudy:
			window.clear(sf::Color::Green);
			break;
			case hot:
			window.clear(sf::Color::Red);
			break;
			case cold:
			window.clear(sf::Color::White);
			break;
			default:
			window.clear(sf::Color::Cyan);
			break;
		}
		window.draw(namae);
		window.draw(start);
		window.draw(scores);
		window.draw(instrucciones);
		window.draw(extra);
		window.display();
	}
}
void Game::Credits(sf::RenderWindow &window) 
{
	sf::Font actualFont;
	sf::Font fancyFont;
	fancyFont.loadFromFile("Assets/Spooky-Spiders.ttf");
	actualFont.loadFromFile("Assets/Crimson-Roman.ttf");
	sf::Text autor("By:Perez Loguzzo Alejo", fancyFont, 35);
	sf::Text materia("Programacion III", actualFont, 25);
	sf::Text tools("Tools used :Visual Studio 2017 , Piskel", actualFont, 15);
	sf::Text used1("Ost: http://www.bensound.com/royalty-free-music/track/jazzy-frenchy", actualFont, 15);
	sf::Text used2(" Sound Effect: http://soundbible.com/511-Squish-1.html", actualFont, 15);
	sf::Text used3("Font: http://www.1001fonts.com/times-new-roman-fonts.html?page=1&items=10", actualFont, 15);
	sf::Text used4("Fancy Font: http://www.dafont.com/es/spooky-spiders.font", fancyFont, 13);
	autor.setFillColor(sf::Color::Magenta);
	tools.setFillColor(sf::Color::Black);
	used1.setFillColor(sf::Color::Black);
	used2.setFillColor(sf::Color::Black);
	used3.setFillColor(sf::Color::Black);
	used4.setFillColor(sf::Color::Black);
	autor.move(150.0f, 100.0f);
	tools.move(100.0f, 250.0f);
	used1.move(100.0f, 300.0f);
	used2.move(100.0f, 350.0f);
	used3.move(100.0f, 400.0f);
	used4.move(100.0f, 450.0f);
	while (window.isOpen() && !(sf::Keyboard::isKeyPressed(sf::Keyboard::V))) {
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			}
		}
		window.clear(sf::Color::White);
		window.draw(autor);
		window.draw(tools);
		window.draw(used1);
		window.draw(used2);
		window.draw(used3);
		window.draw(used4);
		window.display();
	}
}
void Game::Play(sf::RenderWindow &window){
	ost.openFromFile("Assets/Jazzy.ogg");
	ost.play();
	spider->SetHealth(normalHealth);
	for (int i = 0; i < TOPE; i++) 
	{
		if (enemies[i] != NULL) {
			enemies[i]->PositionNow(spider);
		}
	}
	timePassed = warudo.restart();
	while (window.isOpen() && !(spider->Death()) && isPlaying == true) {
		sf::Event event;
		timePassed = warudo.restart();
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}
		spider->Move(timePassed);
		spider->Shoot(proyectile);
		proyectile->Path(window, timePassed);

		for (int i = 0; i < TOPE; i++)
		{
			if (enemies[i] != NULL) {
				enemies[i]->Attack(timePassed, spider);
				enemies[i]->CollideShoot(proyectile);
				if (spider->GetShape().getGlobalBounds().intersects(enemies[i]->GetShape().getGlobalBounds()) &&
					!(enemies[i]->Death())) {
					spider->SetHealth(spider->GetHealth() - enemies[i]->GetHealth());
					enemies[i]->SetHealth(enemies[i]->GetHealth() - enemies[i]->GetHealth());
				}
				enemies[i]->posCombat(spider);
			}
		}
		window.clear(sf::Color::White);
		window.draw(spider->GetShape());
		window.draw(proyectile->GetShape());
		for (int i = 0; i < TOPE; i++)
		{
			if (enemies[i] != NULL) {
				window.draw(enemies[i]->GetShape());
			}
		}
		window.display();
		score++;
	}
	if (score > highScore)
	{
		highScore = score;
		points.open("puntos.txt");
		points << highScore;
		points.close();
	}
	ost.stop();
	isPlaying = false;
	score = 0;
	window.clear(sf::Color::White);
	proyectile->SetThrown(false);
}
