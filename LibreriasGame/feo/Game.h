#ifndef GAME_H
#define GAME_H

#include<iostream>
#include<ctime>
#include<fstream>
#include"SFML/Window.hpp"
#include"SFML/Graphics.hpp"
#include"SFML/Audio.hpp"
#include"SFML/Network.hpp"
#include"SFML/Main.hpp"
#include"Character.h"
#include"Wasp.h"
#include"Bug.h"
#include"Web.h"
#include "Combat.h"
#include"Clima.h"
#if DEBUG
#include"vld.h"
#else
#include<Windows.h>
#endif
#include"json.hpp"
#define TOPE 10
#define CLIMA 800
using namespace std;
using namespace nlohmann;
class Game
{
private:
	sf::Http HostWeb;
	sf::Http::Request hostRequest;
	sf::Http::Response seinResponse; 
	json datus;
	bool isPlaying;
	Character* spider;
	Combat* enemies[TOPE];
	Web* proyectile;
	sf::Clock warudo;
	sf::Time timePassed;
	sf::Music ost;
	sf::Font letra;
	sf::Text texto;
	int score;
	int highScore;
	string detector;
	string pointsFind;
	fstream points;
	int clima;
	const int normalHealth = 110;
	const int ventanaY = 600;
	const int ventanaX = 800;
public:
	Game();
	~Game();
	void Menu();
	void Play(sf::RenderWindow &window);
	void Credits(sf::RenderWindow &window);
};

#endif
