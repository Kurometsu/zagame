#include "Wasp.h"

Wasp::Wasp():Combat(){
	zaTexture.loadFromFile("Assets/Wasp.png");
	rectSourceSprite.contains(32, 32);
	bascSprite.setTexture(zaTexture);
	bascSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
}
Wasp::Wasp(int vida, float tiempo):Combat(vida, tiempo){
	zaTexture.loadFromFile("Assets/Wasp.png");
	rectSourceSprite.contains(32, 32);
	bascSprite.setTexture(zaTexture);
	bascSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
}
Wasp::~Wasp(){
}
void Wasp::Attack(sf::Time tempo, Character* spaider){
	if (!Death()) {
		if (enemyTime <= internalTimer) {
			enemyTime += tempo.asSeconds();
		}
		else if (enemyTime > internalTimer) {
			enemyTime = 0;
		}
		if (spaider->GetShape().getPosition() != bascSprite.getPosition()) {
			if (enemyTime <= subTimer) {
				if (spaider->GetShape().getPosition().x > bascSprite.getPosition().x) {
					bascSprite.move(speed * tempo.asSeconds(), 0);
				}
				else if (spaider->GetShape().getPosition().x < bascSprite.getPosition().x) {
					bascSprite.move(-speed * tempo.asSeconds(), 0);
				}
			}
			else if (enemyTime > subTimer && enemyTime <= internalTimer) {
				if (spaider->GetShape().getPosition().y > bascSprite.getPosition().y) {
					bascSprite.move(0, speed * tempo.asSeconds());
				}
				else if (spaider->GetShape().getPosition().y < bascSprite.getPosition().y) {
					bascSprite.move(0, -speed * tempo.asSeconds());
				}
			}
		}
	}
}