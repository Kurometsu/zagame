#ifndef WASP_H
#define WASP_H

#include "Combat.h"
class Wasp :public Combat {
private:
	const int internalTimer = 4;
	const int subTimer = internalTimer / 2;
	const float speed = 150.0f;
public:
	Wasp();
	Wasp(int vida, float tiempo);
	~Wasp();
	void Attack(sf::Time tempo, Character* spaider);
};

#endif