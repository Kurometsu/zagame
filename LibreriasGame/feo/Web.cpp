#include "Web.h"

Web::Web()
:
look(0),
thrown(false),
thyTexture(),
seinSprite(){
	thyTexture.loadFromFile("Assets/web.png");
	rectSourceSprite.contains(32, 32);
	seinSprite.setTexture(thyTexture);
	seinSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	seinSprite.setPosition(ditchedOutPosition, ditchedOutPosition);
}
Web::~Web(){
}
void Web::Path(sf::RenderWindow &windowScreen, sf::Time counter){
	if (thrown == true) {
		switch (look) {
		case 0:
			seinSprite.move(0, -speedMovement * counter.asSeconds());
			break;
		case 1:
			seinSprite.move(speedMovement * counter.asSeconds(), 0);
			break;
		case 2:
			seinSprite.move(0, speedMovement * counter.asSeconds());
			break;
		case 3:
			seinSprite.move(-speedMovement * counter.asSeconds(), 0);
			break;
		}
		if (seinSprite.getPosition().x > windowScreen.getSize().x ||
			seinSprite.getPosition().x < 0 ||
			seinSprite.getPosition().y > windowScreen.getSize().y ||
			seinSprite.getPosition().y < 0) {
			seinSprite.setPosition(ditchedOutPosition, ditchedOutPosition);
			thrown = false;
		}
	}
}
void Web::SetThrown(bool tirado){
	thrown = tirado;
}
void Web::SetLook(int observado){
	look = observado;
}
const bool Web::GetThrown(){
	return thrown;
}
const int Web::GetLook(){
	return look;
}
sf::Sprite& Web::GetShape(){
	return seinSprite;
}
const sf::IntRect Web::GetRect(){
	return rectSourceSprite;
}