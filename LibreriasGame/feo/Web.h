#ifndef WEB_H
#define WEB_H

#include"SFML/Window.hpp"
#include"SFML/Graphics.hpp"
class Web{
private:
	const int ditchedOutPosition = 1000;
	const float speedMovement = 300.0f;
	int look;
	bool thrown;
	sf::Texture thyTexture;
	sf::IntRect rectSourceSprite;
	sf::Sprite seinSprite;
public:
	Web();
	~Web();
	void Path(sf::RenderWindow &ventanita, sf::Time cuento);
	void SetThrown(bool tirado);
	void SetLook(int observado);
	const bool GetThrown();
	const int GetLook();
	sf::Sprite& GetShape();
	const sf::IntRect GetRect();
};

#endif